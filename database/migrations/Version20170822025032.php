<?php

namespace Db\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170822025032 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $table = $schema->createTable('post');
        $table->addColumn('id', 'integer', [
            'autoincrement' => true,
        ]);
        $table->setPrimaryKey(['id']);
        $table->addColumn('title', 'string', [
            'length' => '50',
        ]);
        $table->addColumn('content', 'text');

        return $schema;
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $schema->dropTable('post');
    }
}
