<?php

use Phinx\Seed\AbstractSeed;
use Faker\Factory as Faker;

class PostSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $post = $this->table('post');
        $post->truncate();
        $faker = Faker::create();
        $data = [];
        for ($i = 0; $i < 20; $i++) {
            $data[] = [
                'title'      => $faker->sentence(4),
                'content'     => $faker->paragraph(20),
            ];
        }

        $post->insert($data)
              ->save();
    }
}
