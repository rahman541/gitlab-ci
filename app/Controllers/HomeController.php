<?php
namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;

class HomeController {

	public function index(Request $request, Application $app) {
		$sql = "SELECT * FROM post";
	    $post = $app['db']->fetchAll($sql);

		return $app['twig']->render('index.twig', ['posts' => $post]);
	}

	public function hello(Request $request, Application $app, $name) {
		// dump($app['url_generator']->getContext()->getBaseUrl());die;
		return $app['twig']->render('hello.twig', ['name' => $name]);
	}

	public function info() {
		return phpinfo();
	}

}
