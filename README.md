## Small Web Application

[![pipeline status](https://gitlab.com/rahman5147/gitlab-ci/badges/master/pipeline.svg)](https://gitlab.com/rahman5147/gitlab-ci/commits/master)
[![coverage report](https://gitlab.com/rahman5147/gitlab-ci/badges/master/coverage.svg)](https://gitlab.com/rahman5147/gitlab-ci/commits/master)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE)

- To test with continous integration (CI)  

## Setup 
```
npm install
php art migrations:migrate
vendor\bin\phinx seed:run
```
