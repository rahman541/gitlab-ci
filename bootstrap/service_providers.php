<?php

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_sqlite',
        'path'     => __DIR__.'/../db.sqlite',
    ),
));
$app['db']->setFetchMode(\PDO::FETCH_OBJ);

// Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../resources/view',
));

// Symfony asset
$app->register(new Silex\Provider\AssetServiceProvider(), array(
    'assets.version' => 'v1',
    'assets.version_format' => '%s?version=%s',
    'assets.named_packages' => array(
        'css' => array('version' => 'css2', 'base_path' => '/css'),
        'js' => array('version' => 'v2', 'base_path' => '/js'),
        'images' => array('base_urls' => array(getenv('BASE_URL'))),
    ),
));