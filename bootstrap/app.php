<?php

date_default_timezone_set("Asia/Kuala_Lumpur");
include __DIR__.'/../vendor/autoload.php';

// ENV configuration
$dotenv = new Dotenv\Dotenv(__DIR__ . '/..');
$dotenv->load();

$app = new Silex\Application();
$app['debug'] = true;
include_once __DIR__.'/service_providers.php';
