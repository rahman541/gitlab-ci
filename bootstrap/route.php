<?php

$app->get('/', 'App\Controllers\HomeController::index')->bind('homepage');
$app->get('info', 'App\Controllers\HomeController::info')->bind('info');
$app->get('hello/{name}', 'App\Controllers\HomeController::hello')->bind('hello');
